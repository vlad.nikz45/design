import QtQuick 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
    visible: true
    width: 400
    height: 600
    title: "Login Page"

    Rectangle {
        width: parent.width
        height: parent.height
        color :"#FFFFFF"

        Column {
            anchors.centerIn: parent
            spacing: 10

            TextField {
                id : loginField
                placeholderText: "Username"
                width: 200
                height: 40
                font.pixelSize: 16
                leftPadding: 5
                topPadding: 7
            }

            TextField {
                id: passwordField
                placeholderText: "Password"
                width: 200
                height: 40
                font.pixelSize: 16
                echoMode: TextInput.Password
                leftPadding: 5
                topPadding: 7
            }

            Row {

                Button {
                    text: "Log In"
                    width: 100
                    height: 50
                    background: Rectangle {
                        color: "Gainsboro"
                    }
                }

                Button {
                    text: "Clear"
                    width: 100
                    height: 50
                    background: Rectangle {
                        color: "transparent"
                    }
                    onClicked: {
                        loginField.text = ""
                        passwordField.text = ""
                    }
                }
            }
        }
    }
}
