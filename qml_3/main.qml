import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    minimumWidth: 320
    minimumHeight: 240

    ColumnLayout {
        anchors.fill: parent
        spacing: 5

        RectComponent {
            id: header
            Layout.fillWidth: true
            Layout.preferredHeight: 50
            color: "#F08080"
            customText: "Header"
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
            RectComponent {
                id: content
                width: parent.width * 0.9
                height: parent.height
                anchors.centerIn: parent
                color: "lightblue"
                customText: "Content"
            }
        }

        RowLayout {
            Layout.fillWidth: true
            Layout.preferredHeight: 50
            spacing: 5

            RectComponent {
                id: button1
                Layout.fillWidth: true
                Layout.preferredHeight: 50
                color: "lightgreen"
                customText: "1"
                height: parent.height
                Layout.alignment: Qt.AlignBottom

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        header.customText = "Button 1 Clicked"
                        content.customText = "Button 1 Content"
                        header.opacity = 1.0
                        content.opacity = 1.0
                        button2.color = "lightgray"
                        button3.color = "lightgray"
                        button1.color = "lightgreen"


                    }
                }
            }

            RectComponent {
                id: button2
                Layout.fillWidth: true
                Layout.preferredHeight: 50
                color: "lightblue"
                customText: "2"
                Layout.alignment: Qt.AlignBottom

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        header.customText = "Button 2 Clicked"
                        content.customText = "Button 2 Content"
                        header.opacity = 0.5
                        content.opacity = 0.5
                        button1.color = "lightgray"
                        button3.color = "lightgray"
                        button2.color = "lightblue"

                    }
                }
            }

            RectComponent {
                id: button3
                Layout.preferredHeight: 50
                Layout.fillWidth: true
                color: "lightyellow"
                customText: "3"
                Layout.alignment: Qt.AlignBottom

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        header.customText = "Button 3 Clicked"
                        content.customText = "Button 3 Content"
                        header.opacity = 0.2
                        content.opacity = 0.2
                        button1.color = "lightgray"
                        button2.color = "lightgray"
                        button3.color = "lightyellow"
                    }
                }
            }
        }
    }
}
