import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15


Window {
    id: myWindow
    width: 640
    height: 480
    visible: true
    title: qsTr("Светофор")

    SwipeView {
        id: view
        currentIndex: pageIndicator.currentIndex
        orientation: Qt.Vertical
        anchors.fill: parent
        padding: 10
        MyPage {
            id: firstPage
            comText: "Красный"
            comColor: "red"
        }
        MyPage {
            id: secondPage
            comText: "Желтый"
            comColor: "yellow"
        }
        MyPage {
            id: thirdPage
            comText: "Зеленый"
            comColor: "green"
        }
    }
    PageIndicator {
        id: pageIndicator
        count: view.count
        currentIndex: view.currentIndex
        anchors.bottom: view.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
