import QtQuick 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
    visible: true
    width: 440
    height: 440

    Rectangle {
        id: container
        width: parent.width
        height: parent.height

        ColoredRectangle {
            id: head
            componentWidth: 80
            componentHeight: 80
            componentColor: "#FFD700"
            anchors.centerIn: parent
        }

        ColoredRectangle {
            id: body
            componentWidth: 80
            componentHeight: 120
            componentColor: "#00FF00"
            anchors.top: head.bottom
            anchors.horizontalCenter: head.horizontalCenter
        }

        ColoredRectangle {
            id: leftArm
            componentWidth: 30
            componentHeight: 80
            componentColor: "#800080"
            x: body.x - leftArm.width
            y: body.y + (body.height - leftArm.height) / 2
        }

        ColoredRectangle {
            id: rightArm
            componentWidth: 30
            componentHeight: 80
            componentColor: "#FF69B4"
            x: body.x + body.width
            y: body.y + (body.height - rightArm.height) / 2
        }

        ColoredRectangle {
            id: leftLeg
            componentWidth: 20
            componentHeight: 80
            componentColor: "#0000FF"
            x: body.x - leftLeg.width + 20
            y: body.y + body.height
        }

        ColoredRectangle {
            id: rightLeg
            componentWidth: 20
            componentHeight: 80
            componentColor: "#FFFF00"
            x: body.x + body.width - 20
            y: body.y + body.height
        }
    }
}
