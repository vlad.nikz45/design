import QtQuick 2.15
import QtQuick.Window 2.15

Rectangle {
    id: customRect
    property alias customColor: customRect.color
    property alias customText: customTextItem.text
    property alias customBorderColor: customRect.border.color
    property alias customWidth: customRect.width
    property alias customHeight: customRect.height

    width: parent.width
    height: parent.height * 0.1
    color: "lightgrey"
    border.color: "#cbcbcb"

    Text {
        id: customTextItem
        text: "Content"
        font {
            pointSize: 12
            weight: Font.Bold
        }
        anchors.centerIn: parent
    }
}
