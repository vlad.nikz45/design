import QtQuick 2.15

Rectangle {
    property int componentWidth: 100
    property int componentHeight: 100
    property color componentColor: "red"

    width: componentWidth
    height: componentHeight
    color: componentColor
    border.color: "black"
}
