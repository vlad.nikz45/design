import QtQuick 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    minimumWidth: 320
    minimumHeight: 240

    Item {
        anchors.fill: parent

        RectComponent {
            id: headerRect
            anchors.top: parent.top
            customText: "Header"
        }

        RectComponent {
            id: contentRect
            width: parent.width * 0.9
            height: parent.height * 0.6
            border.color: "black"
            anchors.centerIn: parent
            customText: "Content"
        }

        Item {
            width: parent.width
            height: parent.height * 0.1
            anchors.bottom: parent.bottom

            RectComponent {
                id: rect1
                height: parent.height
                width: (parent.width - 20) / 3
                customText: "1"
            }

            RectComponent {
                id: rect2
                height: parent.height
                width: (parent.width - 10) / 3
                anchors.left: rect1.right
                anchors.leftMargin: 5
                customText: "2"
            }

            RectComponent {
                height: parent.height
                width: (parent.width - 10) / 3
                anchors.left: rect2.right
                anchors.leftMargin: 5
                customText: "3"
            }
        }
    }
}
