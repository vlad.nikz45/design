import QtQuick 2.15
import QtQuick.Window 2.3
import QtQuick.Layouts 1.5

Window {
    visible: true
    width: 300
    height: 500

    ColumnLayout {
        anchors.fill: parent
        spacing: 10 // пространство между блоками

        Comp {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        Comp {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        Comp {
            Layout.fillWidth: true
            Layout.fillHeight: true
        }
    }
}
