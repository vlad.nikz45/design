import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    minimumWidth: 320
    minimumHeight: 240

    ColumnLayout {
        anchors.fill: parent
        spacing: 5

        RectComponent {
            Layout.fillWidth: true
            Layout.preferredHeight: 50
            color: "#F08080"
            customText: "Header"
            border.width: 2
            customBorderColor: "gray"
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
            RectComponent {
                width: parent.width * 0.9
                height: parent.height
                anchors.centerIn: parent
                border.width: 2
                customBorderColor: "gray"
                color: "lightblue"
                customText: "Content"
            }
        }

        RowLayout {
            Layout.fillWidth: true
            Layout.preferredHeight: 50
            spacing: 5

            RectComponent {
                Layout.fillWidth: true
                Layout.preferredHeight: 50
                color: "lightgreen"
                customText: "1"
                height: parent.height
                border.width: 2
                customBorderColor: "gray"
                Layout.alignment: Qt.AlignBottom
            }

            RectComponent {
                border.width: 2
                customBorderColor: "gray"
                Layout.fillWidth: true
                Layout.preferredHeight: 50
                color: "lightblue"
                customText: "2"
                Layout.alignment: Qt.AlignBottom
            }

            RectComponent {
                border.width: 2
                customBorderColor: "gray"
                Layout.preferredHeight: 50
                Layout.fillWidth: true
                color: "lightyellow"
                customText: "3"
                Layout.alignment: Qt.AlignBottom
            }
        }
    }
}
