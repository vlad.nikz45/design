import QtQuick 2.15

Rectangle {
    property string name: "?"
    property string surname: "?"
    property string message: "?"
    property string time: "?"

    width: parent.width
    height: 80



    Column {
        anchors.fill: parent
        padding: 10
        spacing: 6

        Row {
            spacing: 6
            Text {
                text: name
            }
            Text {
                text: surname
            }
        }

        Text {
            text: message
            wrapMode: Text.WordWrap
            width: parent.width - 20
            horizontalAlignment: Text.AlignLeft
        }

        Row {
            spacing: 6

            Text {
                text: time
            }
            rightPadding: 10
            topPadding: 5
            anchors {
                right: parent.right
            }
        }
    }
}
