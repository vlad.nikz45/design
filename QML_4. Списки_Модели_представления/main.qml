import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
    visible: true
    width: 360
    height: 640
    title: "Messenger"
    color: "lightgreen"

    Rectangle {
        width: parent.width
        height: parent.height
        gradient: Gradient {
            GradientStop { position: 0.0; color: "lightblue" }
            GradientStop { position: 1.0; color: "darkblue" }
        }
    }

    ListModel {
        id: my_model
        ListElement { name: "Ivan"; surname: "Petrushin"; message: "Леди и джентльмены!"; time: "13:00" }
        ListElement { name: "Artem"; surname: "Vesnin"; message: "Я запустил вам тест"; time: "14:00" }
        ListElement { name: "Sergey"; surname: "Vdovin"; message: "Запустите мне, пожалуйста"; time: "19:00" }
    }

    Component {
        id: my_delegate
        MyDelegate {
            name: model.name
            surname: model.surname
            message: model.message
            time: model.time
        }
    }

    ListView {
        id: my_list
        width: parent.width - 30
        height: parent.height - 30
        anchors.centerIn: parent
        model: my_model
        delegate: my_delegate
        spacing: 10
    }

    Rectangle {
        width: parent.width - sendButton.width
        height: 50
        color: "gray"
        border.color: "black"
        border.width: 1
        anchors {
            left: parent.left
            bottom: parent.bottom
        }

        TextField {
            id: messageInput
            anchors.fill: parent
            placeholderText: "Type a message..."
            font.pointSize: 10
            color: "black"
        }
    }

    Button {
        id: sendButton
        height: 50
        width: 70
        anchors {
            bottom: parent.bottom
            right: parent.right
        }
        text: "Send"

        onClicked: {
            my_model.append({
                               name: "New",
                               surname: "User",
                               message: messageInput.text,
                               time: Qt.formatTime(new Date(), "hh:mm")
                           });
            messageInput.text = "";
        }
    }
}
